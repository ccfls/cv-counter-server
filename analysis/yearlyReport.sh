#!/bin/bash

theyear=`date +%Y`

# This needs heirloom-mailx
# General email content
subject="Yearly Patron Count Report $theyear"

# generate the report!
Rscript yearlyReport.r 

# load in dictionary of library names and email addresses
source emails.sh

# iterate through libraries
for recipient in "${!emails[@]}"
do 
    # lookup library email address from dictionary
    email=${emails["$recipient"]}
    # declare a list to store attachment filenames
    declare -a attargs=()
    # iterate through files that contain the word yearly
    for f in yearly*.p*
    do
        # add these files to the list of attachments
         attargs+=( "-a"  "$f" )
    done

    # send the email
    mutt -e 'set content_type=text/html' -s "$subject" "$email" < yearlyTable.html

    sleep 1    

done 
# clean up the mess
rm *yearly*.png
rm *yearly*.html
