#!/bin/bash

thedate=`date +%D`

# This needs heirloom-mailx
# General email content
subject="Patron Count Report $thedate"

# generate the report!
Rscript dailyReport.r 

# load in dictionary of library names and email addresses
source emails.sh

# iterate through libraries
for recipient in "${!emails[@]}"
do 
    # lookup library email address from dictionary
    email=${emails["$recipient"]}
    # declare a list to store attachment filenames
    declare -a attargs=()
    # iterate through files that contain the word daily
    for f in daily*.p*
    do
        # add these files to the list of attachments
         attargs+=( "-a"  "$f" )
    done

    # TODO attach graphs
    # send the email
    mutt -e 'set content_type=text/html' -s "$subject" "$email" < dailyTable.html

    sleep 1    

done 
# clean up the mess
rm *daily*.png
rm *daily*.html
