library(RMySQL)
library(ggplot2)
library(scales)
library(knitr)
library(htmlTable)

# used to store db info
source("config.r")

mydb = dbConnect(MySQL(), user=dbuser, password=dbpswd, dbname=dbname, host=dbaddr, port=dbport)

# get data for current day
rs = dbSendQuery(mydb, "select * from DayCount")
dat = fetch(rs, n=-1)
time = as.POSIXct(dat$Time)

bins <- cut(time,breaks=seq(as.POSIXct(paste(as.Date(time[1]),'00:00',sep=" ")),by='1 hour',length=25))

abbr <- strftime(bins,format='%H:%M')

p3 <- ggplot(dat, aes(x=abbr,y=1.0*..count..,fill=Place)) +
    ggtitle(paste("Number of Patrons by Hour for",Sys.Date(),sep=" ")) +
    xlab("Time") +
    ylab("Patrons")

png("dailyGraph.png")
print(p3+geom_histogram(alpha=0.5))
dev.off()

# make and write table
df <- data.frame(Time=abbr,Place=dat$Place)
t <- table(df)
tm <- addmargins(t)
out <- htmlTable(tm,total=TRUE)
outs <- knit_print(out)[1]

fileConn<-file("dailyTable.html")
writeLines(outs, fileConn)
close(fileConn)
