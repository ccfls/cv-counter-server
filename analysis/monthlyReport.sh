#!/bin/bash

themonth=`date +%Y-%M`

# This needs heirloom-mailx
# General email content
subject="Monthly Patron Count Report $themonth"

# generate the report!
Rscript monthlyReport.r 

# load in dictionary of library names and email addresses
source emails.sh

# iterate through libraries
for recipient in "${!emails[@]}"
do 
    # lookup library email address from dictionary
    email=${emails["$recipient"]}
    # declare a list to store attachment filenames
    declare -a attargs=()
    # iterate through files that contain the word monthly
    for f in monthly*.p*
    do
        # add these files to the list of attachments
         attargs+=( "-a"  "$f" )
    done

    # send the email
    mutt -e 'set content_type=text/html' -s "$subject" "$email" < monthlyTable.html

    sleep 1    

done 
# clean up the mess
rm *monthly*.png
rm *monthly*.html
