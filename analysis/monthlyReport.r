library(RMySQL)
library(ggplot2)
library(scales)
library(knitr)
library(htmlTable)

# used to store db info
source("config.r")

mydb = dbConnect(MySQL(), user=dbuser, password=dbpswd, dbname=dbname, host=dbaddr, port=dbport)

# get data for current month
rs = dbSendQuery(mydb, "select * from MonthCount")
dat = fetch(rs, n=-1)

abbr <- as.Date(dat$Time)

p3 <- ggplot(dat, aes(x=abbr,y=0.5*..count..,fill=dat$Place))  + 
    ggtitle(paste("Number of Patrons by Day for",format(Sys.Date(),"%Y-%m"),sep=" ")) +
    xlab("Time") +
    ylab("Patrons")

png("monthlyGraph.png")
print(p3+geom_histogram(alpha=0.5,binwidth=1))
dev.off()

# make and write table
df <- data.frame(Time=abbr,Place=dat$Place)
t <- table(df)
tm <- addmargins(t)
out <- htmlTable(tm,total=TRUE)
outs <- knit_print(out)[1]

fileConn<-file("monthlyTable.html")
writeLines(outs, fileConn)
close(fileConn)
