library(RMySQL)
library(ggplot2)
library(scales)
library(knitr)
library(htmlTable)

# used to store db info
source("config.r")

mydb = dbConnect(MySQL(), user=dbuser, password=dbpswd, dbname=dbname, host=dbaddr, port=dbport)

# get data for all year
rs = dbSendQuery(mydb, "select * from YearCount")
data = fetch(rs, n=-1)
abbr <- format(as.Date(data$Time),"%Y-%m")


p3 <- ggplot(data, aes(x=abbr,y=0.5*..count..,fill=data$Place)) + 
    ggtitle(paste("Number of Patrons by for",format(Sys.Date(),"%Y"),sep=" ")) +
    xlab("Time") +
    ylab("Patrons")

png("yearlyGraph.png")
print(p3+geom_histogram(alpha=0.5))
dev.off()

# make and write table
df <- data.frame(Time=abbr,Place=data$Place)
t <- table(df)
tm <- addmargins(t)
out <- htmlTable(tm,total=TRUE)
outs <- knit_print(out)[1]

fileConn<-file("yearlyTable.html")
writeLines(outs, fileConn)
close(fileConn)
