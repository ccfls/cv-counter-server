#!/bin/bash

cd /home/counterreports/cv-counter-server/analysis

# first, run the daily report, since this must happen
# every day
./dailyReport.sh

# now check if we need to run the monthly report
TODAY=`date +%d`
TOMORROW=`date +%d -d "1 day"`

# See if tomorrow's day is less than today's
if [ $TOMORROW -lt $TODAY ]; then
./monthlyReport.sh
fi

# now check if we need to run the yearly report
TODAY=`date +%Y`
TOMORROW=`date +%Y -d "1 day"`

# See if tomorrow's day is less than today's
if [ $TOMORROW -gt $TODAY ]; then
./yearlyReport.sh
fi
