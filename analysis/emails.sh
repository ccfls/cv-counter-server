#!/bin/bash

declare -A emails

emails=(["aporter"]="aporter@ccfls.org"
        ["cmurdock"]="cmurdock@ccfls.org"
        ["mlminnis"]="mlminnis@ccfls.org"
)
