-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: picounter
-- ------------------------------------------------------
-- Server version	5.5.49-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AllCounts`
--

DROP TABLE IF EXISTS `AllCounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AllCounts` (
  `Time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Place` char(20) NOT NULL,
  `Library` char(20) NOT NULL,
  `Entry` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Time`,`Place`),
  KEY `Place` (`Place`),
  KEY `fk_library` (`Library`),
  CONSTRAINT `AllCounts_ibfk_1` FOREIGN KEY (`Place`) REFERENCES `Places` (`Name`),
  CONSTRAINT `fk_library` FOREIGN KEY (`Library`) REFERENCES `Libraries` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `DayCount`
--

DROP TABLE IF EXISTS `DayCount`;
/*!50001 DROP VIEW IF EXISTS `DayCount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `DayCount` (
  `Time` tinyint NOT NULL,
  `Place` tinyint NOT NULL,
  `Library` tinyint NOT NULL,
  `Entry` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Libraries`
--

DROP TABLE IF EXISTS `Libraries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Libraries` (
  `Name` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `MonthCount`
--

DROP TABLE IF EXISTS `MonthCount`;
/*!50001 DROP VIEW IF EXISTS `MonthCount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `MonthCount` (
  `Time` tinyint NOT NULL,
  `Place` tinyint NOT NULL,
  `Library` tinyint NOT NULL,
  `Entry` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Places`
--

DROP TABLE IF EXISTS `Places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Places` (
  `Name` char(20) NOT NULL DEFAULT '',
  `Weighting` int(11) DEFAULT NULL,
  `Library` char(20) NOT NULL,
  PRIMARY KEY (`Name`),
  KEY `fk_library2` (`Library`),
  CONSTRAINT `fk_library2` FOREIGN KEY (`Library`) REFERENCES `Libraries` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `YearCount`
--

DROP TABLE IF EXISTS `YearCount`;
/*!50001 DROP VIEW IF EXISTS `YearCount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `YearCount` (
  `Time` tinyint NOT NULL,
  `Place` tinyint NOT NULL,
  `Library` tinyint NOT NULL,
  `Entry` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `DayCount`
--

/*!50001 DROP TABLE IF EXISTS `DayCount`*/;
/*!50001 DROP VIEW IF EXISTS `DayCount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`picounter`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `DayCount` AS select `AllCounts`.`Time` AS `Time`,`AllCounts`.`Place` AS `Place`,`AllCounts`.`Library` AS `Library`,`AllCounts`.`Entry` AS `Entry` from `AllCounts` where ((year(`AllCounts`.`Time`) = year(curdate())) and (month(`AllCounts`.`Time`) = month(curdate())) and (dayofmonth(`AllCounts`.`Time`) = dayofmonth(curdate()))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `MonthCount`
--

/*!50001 DROP TABLE IF EXISTS `MonthCount`*/;
/*!50001 DROP VIEW IF EXISTS `MonthCount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`picounter`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `MonthCount` AS select `AllCounts`.`Time` AS `Time`,`AllCounts`.`Place` AS `Place`,`AllCounts`.`Library` AS `Library`,`AllCounts`.`Entry` AS `Entry` from `AllCounts` where ((year(`AllCounts`.`Time`) = year(curdate())) and (month(`AllCounts`.`Time`) = month(curdate()))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `YearCount`
--

/*!50001 DROP TABLE IF EXISTS `YearCount`*/;
/*!50001 DROP VIEW IF EXISTS `YearCount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`picounter`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `YearCount` AS select `AllCounts`.`Time` AS `Time`,`AllCounts`.`Place` AS `Place`,`AllCounts`.`Library` AS `Library`,`AllCounts`.`Entry` AS `Entry` from `AllCounts` where (year(`AllCounts`.`Time`) = year(curdate())) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-25 10:03:50
